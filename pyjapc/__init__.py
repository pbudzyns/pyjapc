import warnings

from ._japc import PyJapc
try:
    from .rbac_dialog import PasswordEntryDialogue, getPw
except ImportError:
    warnings.warn('Unable to import rbac_dialog Tkinter tools.')
from ._version import version as __version__


__cmmnbuild_deps__ = [
    "accsoft-commons-domain",
    "japc",
    "japc-ext-cmwrda3",  # (indirect use) For RDA communication
    "japc-ext-inca",
    "japc-ext-mockito2",
    "japc-ext-remote",  # (indirect use)
    "japc-ext-tgm",
    "japc-svc-ccs",  # (indirect use) To fetch descriptors without INCA
    "japc-value",
    "log4j",
    "rbac-client",
    "rbac-util",
    "slf4j-api",
    "slf4j-log4j12",
]
