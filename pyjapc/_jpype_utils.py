"""
Utils for working with JPype in PyJapc.

"""
from distutils.version import LooseVersion as _LooseVersion
import gc

import jpype as jp


JPYPE_LT_0p7 = _LooseVersion(jp.__version__) < _LooseVersion('0.7')


def fetch_property(method):
    # Jpype1 <=0.7 used to automatically expose some methods as
    # properties. Newer versions of Jpype1 are more explicit and require
    # the user to call them directly.
    if JPYPE_LT_0p7:
        return method
    else:
        return method()


class JavaGCCollector(object):
    """
    An object to trigger the Java System garbage collection when the Python
    garbage collection has run.

    The following code will ensure that the next time the Python GC runs, the
    Java GC will be triggered:

        >>> JavaGCCollector().trigger()

    If you want to ensure that at most only a single Java GC call is made for a
    Python GC cycle, create a the collector and successively call its trigger
    method:

        >>> collector = JavaGCCollector()
        >>> collector.trigger()
        # Even if you call trigger again, only a single Java GC call will be
        # made when the Python GC next runs.
        >>> collector.trigger()

    """
    def __init__(self):
        self._added = False

    def trigger(self):
        """
        Setup the Java GC to run when the Python GC next runs.

        """
        if not self._added:
            gc.callbacks.append(self._gc_callback)
        self._added = True

    def _gc_callback(self, phase, info):
        _ = phase, info

        # Trigger the Java garbage collection.
        if jp.isJVMStarted() and not JPYPE_LT_0p7:
            jp.java.lang.System.gc()

        gc.callbacks.remove(self._gc_callback)
        self._added = False
