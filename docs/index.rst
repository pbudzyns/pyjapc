
.. include:: ../README.rst

Contents
========

.. toctree::
    :maxdepth: 4

    pyjapc
    examples


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`

.. include:: ../CHANGELOG.rst
