PyJapc
======

PyJapc is a Python to FESA/LSA/INCA interface via JAPC.

Full documentation and examples are available at:

https://acc-py.web.cern.ch/gitlab/scripting-tools/pyjapc/docs/stable/


And on the Scripting Tools wiki at:

https://wikis.cern.ch/display/ST/PyJapc


Testing
=======

To run test, install required libraries::

    pip install .[test]


Then run tests::

    pytest ./pyjapc

